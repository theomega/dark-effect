#!/usr/bin/env python
from PIL import Image
import argparse
import sys

# Parse the arguments and options
parser = argparse.ArgumentParser(description='Dark Effect')
parser.add_argument('--offset', default=0, type=int,
  help='X-Axis (left) offset for the cropped source for the effect, defaults to 0')
parser.add_argument('--flip', action='store_const', default=False, const=True,
  help='Flips the source crop before using it as the first stripe')
parser.add_argument('--stripeno', default=4, type=int,
  help='Defines how many stripes should be printed, defaults to 4')
parser.add_argument('--output', '-o',
  help='Output file to store the result to, if not provided, the output is displayed instead')
parser.add_argument('file',
  help='Input file')

args = parser.parse_args()

# Validate the options
if (args.stripeno < 2):
  print("Number of stripes parameter needs to be at least 2, is %d" % (args.stripeno))
  sys.exit(1)

# Read the input file
im = Image.open(args.file)

# Calcuate the strip width by dividing the width of the image by the number of stripes
strip_width = int(im.size[0] / float(args.stripeno))

# Print some debugging infos
print("format: %s, size: %s, mode: %s, strip_width: %d, stripe no: %d, offset: %d" % (im.format, im.size, im.mode, strip_width, args.stripeno, args.offset))

# Validate that the source stripe is still within the image boundaries
if (args.offset + strip_width > im.size[0]):
  print("Offset too large, maximum offset is %d" % (im.size[0] - strip_width))
  sys.exit(1)

# Cut out the source stripe
source = im.crop((args.offset, 0, args.offset + strip_width, im.size[1]))

if args.flip:
  # Start the output with a flipped stripe?
  source = source.transpose(Image.FLIP_LEFT_RIGHT)

for i in range(0, args.stripeno):
  # Paste the source stripe at the right position on the target image
  im.paste(source, (i * strip_width, 0, (i + 1) * strip_width, im.size[1]))
  # Flip the source stripe after each iteration, this means that every second run
  # the original stripe is used again
  source = source.transpose(Image.FLIP_LEFT_RIGHT)

# If no output file was defined, show the image, otherwise save to the file
if not args.output:
  im.show()
else:
  im.save(args.output)
