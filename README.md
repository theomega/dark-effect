Dark Effect - Still Image Kaleidoscope Effect
=============================================

Idea
----
Replicate the mirror/kaleidoscope effect similar as the one used heavily in the [intro](https://www.youtube.com/watch?v=8c399HPb01s) of the [Netflix series "Dark"](https://www.netflix.com/de-en/title/80100172) for still images. Technologies used are python and pil/pillow.

Sample
------
Click on each of the images for a bigger version

### Input:
[![Input](samples/original_small.jpg)](samples/original.jpg)

### 4 parts, offset 0
[![4 parts, offset 0](samples/result_4_0_small.jpg)](samples/result_4_0.jpg)

`./main.py samples/original.jpg`

### 4 parts, offset 0, flipped piece
[![4 parts, offset 0, flipped piece](samples/result_4_0_flip_small.jpg)](samples/result_4_0_flip.jpg)

`./main.py --flip samples/original.jpg` 

### 4 parts, offset 1500
[![4 parts, offset 1500](samples/result_4_1500_small.jpg)](samples/result_4_1500.jpg)

`./main.py --offset 1500 samples/original.jpg` 

### 8 parts, offset 1500
[![8 parts, offset 1500](samples/result_8_1500_small.jpg)](samples/result_8_1500.jpg)

`./main.py --offset 1500 --partno 8 samples/original.jpg` 

### Video Generation
![Generated Video](samples/result_video.mp4)

By generating several frames with slightliy changing offsets, croping them and joining them together, a nice video effect can be created.


Effect algorithm
----------------
The effect is quite trivial: It divides the output image into several stripes. The number of stripes defaults to 4, but can be changed. The left most stripe is copied 1:1 from the source image. Per default the left most part of the source image is used for this. Using the `offset` parameter, other parts of the source image can be choosen. Every succeeding stripe is a flipped copy of the stripe left to it. Using the `flip` parameter, the first initial stripe can be flipped to change the effect order. The resulting image has the same dimensions as the source image.

Usage
-----
```
git clone https://gitlab.com/theomega/dark-effect.git
cd dark-effect
python3 -m venv .
source bin/activate
pip install -r requirements.txt
./main.py samples/original.jpg
```

Ideas / TODO
------------
- Integrate as plugin into gimp
- Testing
- Better documentation
- Vertical mode
- Combined vertical and horizontal mode
- Simple UI to determine parameters
- Webbased version

Alternatives
------------
- Adobes Photoshop Express has a similar effect directly integrated
- Any image editing software can be used to get the effect
